//
//  FeedCell.swift
//  Scheduler
//
//  Created by Joash Tubaga on 01/11/2017.
//  Copyright © 2017 Joash Tubaga. All rights reserved.
//

import UIKit
import LBTAComponents

class BaseCell: UICollectionViewCell {
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupView()
  }
  
  func setupView() {
    
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

class FeedCell: BaseCell {
  
  lazy var collectionView: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
    cv.backgroundColor = .white
    cv.delegate = self
    cv.dataSource = self
    return cv
  }()
  
  override func setupView() {
    let nib = UINib(nibName: "ScheduleTaskCell", bundle: nil)
    collectionView.register(nib, forCellWithReuseIdentifier: "cell")
    addSubview(collectionView)
    collectionView.anchor(topAnchor,
                      left: leftAnchor,
                      bottom: bottomAnchor,
                      right: rightAnchor,
                      topConstant: 0,
                      leftConstant: 0,
                      bottomConstant: 0,
                      rightConstant: 0,
                      widthConstant: 0,
                      heightConstant: 0)
  }
  
}

extension FeedCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 20
  }

   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ScheduleTaskCell
    cell.backgroundColor = .clear
    return cell
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width:
      frame.width, height: 128)
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 1
  }
}

