//
//  ScheduleTaskCell.swift
//  Scheduler
//
//  Created by Joash Tubaga on 02/11/2017.
//  Copyright © 2017 Joash Tubaga. All rights reserved.
//

import UIKit

class ScheduleTaskCell: UICollectionViewCell {
  
  @IBOutlet weak var cellBackgroundView: UIView! {
    didSet {
      cellBackgroundView.layer.cornerRadius = 3
    }
  }
  @IBOutlet weak var activityIconImage: UIView! {
    didSet {
      activityIconImage.layer.cornerRadius = 5
    }
  }
  @IBOutlet weak var timeScheduleLabel: UILabel!
  @IBOutlet weak var TitleLabel: UILabel!
  @IBOutlet weak var profileNameLabel: UILabel!
  @IBOutlet weak var currentLocationNameLabel: UILabel!
  @IBOutlet weak var profileImage: UIImageView! {
    didSet {
      profileImage.layer.cornerRadius = 23
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
}
