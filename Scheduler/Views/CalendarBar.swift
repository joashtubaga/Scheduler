//
//  CalendarBar.swift
//  Scheduler
//
//  Created by Joash Tubaga on 01/11/2017.
//  Copyright © 2017 Joash Tubaga. All rights reserved.
//

import UIKit
import LBTAComponents

protocol CalendarBarDelegate {
  func scrollToCalendar(at index: Int)
}

class CalendarBar: UIView {
  
  let calendarBlue = UIColor.init(r: 23, g: 66, b: 131)
  let calendarPick = UIColor.init(r: 209, g: 57, b: 84)
  let days: [String] = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
  let date: [Int] = [1, 2, 3, 4, 5, 6, 7]
  
  var delegate: CalendarBarDelegate? = nil
  
  lazy var collectionView: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
    cv.backgroundColor = calendarBlue
    cv.delegate = self
    cv.dataSource = self
    return cv
  }()
  
  override init(frame: CGRect) {
    super .init(frame: frame)
    
    collectionView.register(CalendarCell.self, forCellWithReuseIdentifier: "cell")
    
    let selectIndexPath = NSIndexPath(item: 0, section: 0)
    collectionView.selectItem(at: selectIndexPath as IndexPath, animated: false, scrollPosition: .top)
    
    addSubview(collectionView)
    collectionView.anchor(topAnchor,
                          left: leftAnchor,
                          bottom: bottomAnchor,
                          right: rightAnchor,
                          topConstant: 0,
                          leftConstant: 0,
                          bottomConstant: 0,
                          rightConstant: 0,
                          widthConstant: 0,
                          heightConstant: 0)
    
    horizontalBar()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  var horizontalLeftAnchorContraint: NSLayoutConstraint?
  
  func horizontalBar() {
    let horizontalBarView = UIView()
    horizontalBarView.backgroundColor = calendarPick
    horizontalBarView.translatesAutoresizingMaskIntoConstraints = false
    
    addSubview(horizontalBarView)
    horizontalLeftAnchorContraint = horizontalBarView.leftAnchor.constraint(equalTo: leftAnchor)
    horizontalLeftAnchorContraint?.isActive = true
    
    horizontalBarView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    horizontalBarView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/7).isActive = true
    horizontalBarView.heightAnchor.constraint(equalToConstant: 4).isActive = true
  }
}

extension CalendarBar: UICollectionViewDelegate, UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    delegate?.scrollToCalendar(at: indexPath.item)
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return days.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CalendarCell
    cell.dayLabel.text = days[indexPath.row]
    cell.dateLabel.text = String(date[indexPath.row])
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 0
  }
}

extension CalendarBar: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
    return CGSize(width: frame.width / 7, height: frame.height)
  }
}

