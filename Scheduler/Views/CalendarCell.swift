//
//  CalendarCell.swift
//  Scheduler
//
//  Created by Joash Tubaga on 01/11/2017.
//  Copyright © 2017 Joash Tubaga. All rights reserved.
//

import UIKit
import LBTAComponents

class CalendarCell: BaseCell {
  
  let calendarPink = UIColor.init(r: 236, g: 40, b: 75)
  let calendarBlue = UIColor.init(r: 23, g: 66, b: 131)
  var feedController: FeedController?
  
  let dayLabel: UILabel = {
    let label = UILabel()
    label.backgroundColor = .clear
    label.textAlignment = .center
    label.font = UIFont.systemFont(ofSize: 14, weight: .light)
    label.textColor = .white
    return label
  }()
  
  let dateLabel: UILabel = {
    let label = UILabel()
    label.backgroundColor = .clear
    label.textAlignment = .center
    label.font = UIFont.systemFont(ofSize: 25, weight: .bold)
    label.textColor = .white
    return label
  }()
  
  override var isHighlighted: Bool {
    didSet {
      backgroundColor = isHighlighted ? calendarPink : calendarBlue
      
    }
  }
  
  override var isSelected: Bool {
    didSet {
      backgroundColor = isSelected ? calendarPink : calendarBlue
    }
  }
  
  override func setupView() {
    addSubview(dayLabel)
    dayLabel.anchor(topAnchor,
                    left: leftAnchor,
                    bottom: nil,
                    right: rightAnchor,
                    topConstant: 8,
                    leftConstant: 8,
                    bottomConstant: 0,
                    rightConstant: 8,
                    widthConstant: 0,
                    heightConstant: 20)
    
    addSubview(dateLabel)
    dateLabel.anchor(dayLabel.bottomAnchor,
                     left: leftAnchor,
                     bottom: bottomAnchor,
                     right: rightAnchor,
                     topConstant: 4,
                     leftConstant: 8,
                     bottomConstant: 8,
                     rightConstant: 8,
                     widthConstant: 0,
                     heightConstant: 0)
  }
}
