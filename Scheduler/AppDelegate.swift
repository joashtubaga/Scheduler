//
//  AppDelegate.swift
//  Scheduler
//
//  Created by Joash Tubaga on 31/10/2017.
//  Copyright © 2017 Joash Tubaga. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.makeKeyAndVisible()
    
    window?.rootViewController = UINavigationController(rootViewController: FeedController())
    UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    
    statusBarSetup()
    navigationSetup()
    
    return true
  }
  
  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }
  
  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }
  
  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }
  
  private func statusBarSetup() {
    UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    let statusBarBackgroundView = UIView()
    statusBarBackgroundView.backgroundColor = UIColor.init(r: 29, g: 73, b: 136)
    
    window?.addSubview(statusBarBackgroundView)
    statusBarBackgroundView.anchor(window?.topAnchor, left: window?.leftAnchor, bottom: nil, right: window?.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
  }
  
  private func navigationSetup() {
    let navigationBarAppearace = UINavigationBar.appearance()
    navigationBarAppearace.barTintColor = UIColor.init(r: 23, g: 66, b: 131)
    navigationBarAppearace.tintColor = .white
    navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
    navigationBarAppearace.shadowImage = UIImage()
    navigationBarAppearace.setBackgroundImage(UIImage(), for: .default)
  }
}

