//
//  ViewController.swift
//  Scheduler
//
//  Created by Joash Tubaga on 31/10/2017.
//  Copyright © 2017 Joash Tubaga. All rights reserved.
//

import UIKit

class FeedController: UICollectionViewController {
  
  let calendarBarHeight: CGFloat = 70
  
  lazy var calendarBar: CalendarBar = {
    let calendarView = CalendarBar()
    calendarView.delegate = nil
    return calendarView
  }()
  
  
  convenience init() {
    self.init(collectionViewLayout: UICollectionViewFlowLayout())
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationItem.title = "Schedule"
    navigationController?.navigationBar.isTranslucent = false
    
    setupCollectionView()
    setupCalendarBar()
  }
  
  private func setupCollectionView() {
    if let flowLayout = collectionView?.collectionViewLayout as?
      UICollectionViewFlowLayout {
      flowLayout.scrollDirection = .horizontal
      flowLayout.minimumLineSpacing = 0
    }
    
    collectionView?.backgroundColor = .white
    collectionView?.register(FeedCell.self, forCellWithReuseIdentifier: "cell")
    collectionView?.contentInset = UIEdgeInsetsMake(70, 0, 0, 0)
    collectionView?.scrollIndicatorInsets  = UIEdgeInsetsMake(70, 0, 0, 0)
    collectionView?.isPagingEnabled = true
    collectionView?.showsHorizontalScrollIndicator = false
    collectionView?.showsVerticalScrollIndicator = false
    collectionView?.bounces = false
  }
  
  private func setupCalendarBar() {
    calendarBar.delegate = self
    view.addSubview(calendarBar)
    calendarBar.anchor(view.topAnchor,
                       left: view.leftAnchor,
                       bottom: nil,
                       right: view.rightAnchor,
                       topConstant: 0,
                       leftConstant: 0,
                       bottomConstant: 0,
                       rightConstant: 0,
                       widthConstant: 0,
                       heightConstant: calendarBarHeight
    )
  }
  
}

extension FeedController: UICollectionViewDelegateFlowLayout {
  override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    let index = targetContentOffset.pointee.x / view.frame.width
    let indexPath = NSIndexPath(item: Int(index), section: 0)
    calendarBar.collectionView.selectItem(at: indexPath as IndexPath, animated: true, scrollPosition: [])
  }
  
  override func scrollViewDidScroll(_ scrollView: UIScrollView) {
    calendarBar.horizontalLeftAnchorContraint?.constant = scrollView.contentOffset.x / 7
  }
  
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    let numberOfSampleFeedCell = 7
    return numberOfSampleFeedCell
  }
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FeedCell
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
    return CGSize(width: view.frame.width, height: view.frame.height - calendarBarHeight)
  }
  
}

extension FeedController: CalendarBarDelegate {
  func scrollToCalendar(at index: Int) {
    let indexPath = NSIndexPath(item: index, section: 0)
    collectionView?.scrollToItem(at: (indexPath as IndexPath) as IndexPath, at: [], animated: true)
  }
}
